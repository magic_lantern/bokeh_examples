use mimic2;
select concat('select * from ', table_name ,' limit 20;')
from information_schema.tables
where table_schema = 'mimic2';

select * from a_chartdurations limit 20;
select * from a_iodurations limit 20;
select * from a_meddurations limit 20;

select * from additives limit 20;

select * from admissions limit 20;
select * from censusevents limit 20;

select count(1), itemid from chartevents
group by itemid;

select * from d_patients where subject_id = 3;
select * from chartevents where itemid = 51 and subject_id = 3;

  FROM [HandHygiene].[dbo].[compliance_events] compliance
       left outer join [HandHygiene].[dbo].[staff_members] staff on compliance.staff_member_id = staff.id

select c.subject_id, p.dob, c.charttime, c.itemid, c.value1num, c.value2num, FLOOR(DATEDIFF(c.charttime, p.dob)/365) age
from d_patients p left join chartevents c on p.subject_id = c.subject_id
where 
	c.itemid = 51
	and value1num != 0
	and value2num != 0
order by charttime
limit 1000;
-- 51	Arterial BP
-- 52	Arterial BP Mean
select * from comorbidity_scores limit 20;
select * from d_caregivers limit 20;
select * from d_careunits limit 20;

select * from d_chartitems order by itemid;

select * from d_chartitems_detail limit 20;
select * from d_codeditems limit 20;
select * from d_demographicitems limit 20;
select * from d_ioitems limit 20;
select * from d_labitems limit 20;
select * from d_meditems limit 20;
select * from d_parammap_items limit 20;

SELECT FLOOR(DATEDIFF(dod, dob)/365), sex from d_patients limit 20;
describe d_patients;

select * from db_schema limit 20;
select * from deliveries limit 20;
select * from demographic_detail limit 20;
select * from demographicevents limit 20;
select * from drgevents limit 20;
select * from icd9 limit 20;
select * from icustay_days limit 20;
select * from icustay_detail limit 20;
select * from icustayevents limit 20;
select * from ioevents limit 20;

select * from labevents limit 20;
select count(1), itemid
from labevents
group by itemid;

select * from d_labitems where itemid = '50090';

select * from item

select * from medevents limit 20;
select * from microbiologyevents limit 20;
select * from noteevents limit 20;
select * from parameter_mapping limit 20;
select * from poe_med limit 20;
select * from poe_order limit 20;
select * from procedureevents limit 20;
select * from totalbalevents limit 20;

select 
    c.subject_id, 
    p.dob,
    c.charttime,
    c.itemid,
    c.value1num,
    c.value2num,
    p.sex,
    FLOOR(DATEDIFF(c.charttime, p.dob)/365) age,
    FLOOR(DATEDIFF(c.charttime, p.dob)/3650) decade
from d_patients p left join chartevents c on p.subject_id = c.subject_id
where 
	c.itemid = 51
    -- Arterial Blood Pressure
	and value1num != 0
	and value2num != 0
    and FLOOR(DATEDIFF(c.charttime, p.dob)/365) >= 30
    and FLOOR(DATEDIFF(c.charttime, p.dob)/365) <= 40