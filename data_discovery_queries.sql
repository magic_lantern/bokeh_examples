-- query showing how long it actually takes for someone to wash (either before entering room or after entering room or after leaving)
SELECT
compliance.[id] compliance_id
,compliance.[staff_member_id]
,staff.name staff_name
,staff.badge
,roles.name role_name
,staff.role_id
-- ,compliance.[room_id]
,rooms.name room_name
--,compliance.[enter]
, (case when compliance.[enter] = 1 then 'enter' else 'leave' end) as enter_exit
-- ,compliance.[change_time]
,DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), compliance.change_time) AS time
,compliance.[compliant]
,(case when compliance.[enter] = 1 then ABS(DATEDIFF(ss,  compliance.wash_before_time, compliance.change_time)) else ABS(DATEDIFF(ss, compliance.change_time, compliance.wash_after_time)) end) as wash_elapsed_seconds
--,compliance.[wash_before_time]
,DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), compliance.wash_before_time) AS wash_before_time_local
--,compliance.[wash_after_time]
,DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), compliance.wash_after_time) AS wash_after_time_local
,compliance.[late]
--,compliance.[shift_day]
,DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), compliance.shift_day) AS shift_day_local
,compliance.[room_type]
,compliance.[dispenser_type]

  FROM [HandHygiene].[dbo].[compliance_events] compliance
       left outer join [HandHygiene].[dbo].[staff_members] staff on compliance.staff_member_id = staff.id
       left outer join [HandHygiene].[dbo].[roles] roles on roles.id = staff.role_id
       left outer join [HandHygiene].[dbo].[rooms] rooms on compliance.room_id = rooms.id
-- if want to filter results based on a specific date range
--
-- where DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), compliance.change_time) < '2016-04-19'
--and DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), compliance.change_time) >= '2016-04-06'
--where compliance.staff_member_id = '29'
--and rooms.name = '2103'
where compliant = 0
order by compliance.change_time;



/****** get number of rows
SELECT
count(1)
  FROM [HandHygiene].[dbo].[compliance_events] compliance
       left outer join [HandHygiene].[dbo].[staff_members] staff on compliance.staff_member_id = staff.id
       left outer join [HandHygiene].[dbo].[roles] roles on roles.id = staff.role_id
       left outer join [HandHygiene].[dbo].[rooms] rooms on compliance.room_id = rooms.id
          ******/

select * from  [HandHygiene].[dbo].compliance_events;


-- number of compliance events by room_type
select distinct room_type, count(1) from compliance
group by room_type
order by count(1) desc;

-- number of compliance events by role
select distinct role_name, count(1) from compliance
group by role_name
order by count(1) desc;

-- number of compliance events by room
select distinct room_name, count(1) from compliance
group by room_name
order by count(1) desc;
